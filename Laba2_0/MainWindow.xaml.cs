﻿using ExcelDataReader;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Laba2_0.Models;

namespace Laba2_0
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public IEnumerable<Cell> DataSource { get; set; }

        private int Page { get; set; } = 1;
        private int PageSize { get; set; } = 15;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private async void DownloadFile_Click(object sender, RoutedEventArgs e)
        {
            using (var client = new WebClient())
            {
                try
                {
                    var directory = Directory.GetCurrentDirectory();
                    string url = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
                    if (File.Exists(Path.Combine(directory, "thrlist.xlsx")))
                    {
                        await client.DownloadFileTaskAsync(new Uri(url), Path.Combine(directory, "thrlist.xlsx")).ConfigureAwait(false);
                        MessageBox.Show("Файл обновлен!");
                    }
                    else
                    {
                        await client.DownloadFileTaskAsync(new Uri(url), Path.Combine(directory, "thrlist.xlsx")).ConfigureAwait(false);
                        MessageBox.Show("Файл скачен!");
                    }
                }
                catch(Exception msg)
                {
                    LogFile.Log(msg, "Ошибка при скачивание файла! Нет интернет соединения!");
                    MessageBox.Show("Ошибка при скачивание файла!\nПровете интернет соединение!");
                }
            }
        }

        private void CompareFiles_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Directory.GetCurrentDirectory();

            if (openFileDialog.ShowDialog() == true)
            {                
                FileProvider open = new FileProvider();
                var newDataSource = open.OpenFile(openFileDialog.FileName);

                var different = GetDifferentBetweenDataSources(DataSource, newDataSource);

                var stringBuilder = new StringBuilder();
                foreach (CellCompareModel comparedResult in different)
                {
                    stringBuilder.Append($"Было: {comparedResult.Before}\nСтало: {comparedResult.After}\n");
                }

                var result = stringBuilder.ToString();
                if (string.IsNullOrWhiteSpace(result))
                {
                    result = "Между файлами нет разницы!";
                }

                MessageBox.Show(result.Substring(0, result.Length > 1000 ? 1000 : result.Length));
            }
        }

        private IEnumerable<CellCompareModel> GetDifferentBetweenDataSources(IEnumerable<Cell> first, IEnumerable<Cell> second)
        {
            foreach(Cell firstsCell in first)
            {
                var secondsCell = second.FirstOrDefault(x => x.Id == firstsCell.Id);

                var result = firstsCell.Equals(secondsCell);

                if (result)
                {
                    continue;
                }

                yield return new CellCompareModel()
                {
                    Before = firstsCell,
                    After = secondsCell,
                };
            }
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            var directory = Directory.GetCurrentDirectory();
            try
            {
                FileProvider open = new FileProvider();
                DataSource = open.OpenFile("thrlist.xlsx");
                var count = DataSource.Count();

                if(count > 0)
                {
                    CompareFiles.IsEnabled = true;
                }

                DateTime dateTime = File.GetLastWriteTime(Path.Combine(directory, "thrlist.xlsx"));
                DownloadDate.Content = dateTime.ToLongDateString();

                Page = 1;
                FileDataGrid.Columns[2].MaxWidth = 0;
                FileDataGrid.Columns[3].MaxWidth = 0;
                FileDataGrid.Columns[4].MaxWidth = 0;
                FileDataGrid.Columns[5].MaxWidth = 0;
                FileDataGrid.Columns[6].MaxWidth = 0;
                FileDataGrid.Columns[7].MaxWidth = 0;
                FileDataGrid.Columns[8].MaxWidth = 0;
                FileDataGrid.Columns[9].MaxWidth = 0;

                FileDataGrid.ItemsSource = DataSource.ToList()
                    .Skip((Page - 1) * PageSize)
                    .Take(PageSize);
            }
            catch
            {
            }
        }

        private void Before_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Page > 1)
                {
                    Page--;
                }

                FileDataGrid.ItemsSource = DataSource.ToList()
                    .Skip((Page - 1) * PageSize)
                    .Take(PageSize);
            }
            catch
            {
            }
        }

        private void After_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Page - 1 < DataSource.Count() / PageSize)
                {
                    Page++;
                }

                FileDataGrid.ItemsSource = DataSource.ToList()
                    .Skip((Page - 1) * PageSize)
                    .Take(PageSize);
            }
            catch
            {
            }
        }
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            TextBlock td1 = FileDataGrid.Columns[0].GetCellContent(row) as TextBlock;
            TextBlock td2 = FileDataGrid.Columns[1].GetCellContent(row) as TextBlock;
            TextBlock td3 = FileDataGrid.Columns[2].GetCellContent(row) as TextBlock;
            TextBlock td4 = FileDataGrid.Columns[3].GetCellContent(row) as TextBlock;
            TextBlock td5 = FileDataGrid.Columns[4].GetCellContent(row) as TextBlock;
            TextBlock td6 = FileDataGrid.Columns[5].GetCellContent(row) as TextBlock;
            TextBlock td7 = FileDataGrid.Columns[6].GetCellContent(row) as TextBlock;
            TextBlock td8 = FileDataGrid.Columns[7].GetCellContent(row) as TextBlock;
            TextBlock td9 = FileDataGrid.Columns[8].GetCellContent(row) as TextBlock;
            TextBlock td10 = FileDataGrid.Columns[9].GetCellContent(row) as TextBlock;
            var message = $"Общая информация\nИдентификатор УБИ: {td1.Text} \nНаименование УБИ: {td2.Text} \nОписание: {td3.Text}" +
                $"\nИсточник угрозы (характеристика и потенциал нарушителя): {td4.Text}\nОбъект воздействия: {td5.Text}\nПоследствия\n" +
                $"Нарушение конфиденциальности: {td6.Text}\nНарушение целостности: {td7.Text}\nНарушение доступности: {td8.Text}\n" +
                $"Дополнительно\nДата включения угрозы в БДУ: {td9.Text}\nДата последнего изменения данных: {td10.Text}";

            MessageBox.Show(message);
        }

        private async void OpenLogFile_Click(object sender, RoutedEventArgs e)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "log");
            string name = "Laba2_0.exe_LogFile.log";
            try
            {
                using (StreamReader sr = new StreamReader(Path.Combine(path, name), Encoding.GetEncoding(1251)))
                {
                    MessageBox.Show(await sr.ReadToEndAsync());
                }
            }
            catch
            {
                MessageBox.Show("Файла не существует!");
            }
        }
    }
}
