﻿using System;
using System.IO;
using System.Text;

namespace Laba2_0
{
    public class LogFile
    {
        private static object sync = new object();
        public static void Log(Exception ex, string message)
        {
            var directory = Directory.GetCurrentDirectory();
            try
            {
                string path = Path.Combine(directory, "log");
                if (!File.Exists(path))
                {
                    Directory.CreateDirectory(path);
                    string str = Path.Combine(path, string.Format("{0}_LogFile.log", AppDomain.CurrentDomain.FriendlyName));
                    string fullText = string.Format("[{0:dd.MM.yyy HH:mm:ss.fff}] Ошибка:{1}\r\n", DateTime.Now, message);
                    lock (sync)
                    {
                        File.AppendAllText(str, fullText, Encoding.GetEncoding("Windows-1251"));
                    }
                }
            }
            catch
            {
            }
        }
        public static void LogSuc(string message)
        {
            var directory = Directory.GetCurrentDirectory();
            try
            {
                string path = Path.Combine(directory, "log");
                if (!File.Exists(path))
                {
                    Directory.CreateDirectory(path);
                    string str = Path.Combine(path, string.Format("{0}_LogFile.log", AppDomain.CurrentDomain.FriendlyName));
                    string fullText = string.Format("[{0:dd.MM.yyy HH:mm:ss.fff}] Обновление: {1}\r\n", DateTime.Now, message);
                    lock (sync)
                    {
                        File.AppendAllText(str, fullText, Encoding.GetEncoding("Windows-1251"));
                    }
                }
            }
            catch
            {
            }
        }
    }
}
