﻿using ExcelDataReader;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Laba2_0.Models;

namespace Laba2_0
{
    public class FileProvider
    {
        public IEnumerable<Cell> OpenFile(string openFile)
        {
            using (var stream = File.Open(openFile, FileMode.Open, FileAccess.Read))
            {
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                DataSet result = excelReader.AsDataSet();
                DataTable dt = result.Tables[0];

                for (int i = 2; i < dt.Rows.Count; i++)
                {
                    yield return new Cell()
                    {
                        Id = "УБИ." + dt.Rows[i][0].ToString(),
                        Name = dt.Rows[i][1].ToString(),
                        Description = dt.Rows[i][2].ToString(),
                        SourceThreat = dt.Rows[i][3].ToString(),
                        ObjectImpact = dt.Rows[i][4].ToString(),
                        PrivacyViolation = dt.Rows[i][5].ToString(),
                        IntegrityViolation = dt.Rows[i][6].ToString(),
                        AccessViolation = dt.Rows[i][7].ToString(),
                        InclusionDate = dt.Rows[i][8].ToString(),
                        DateChange = dt.Rows[i][9].ToString(),
                    };
                }
            }
        }
    }

}
