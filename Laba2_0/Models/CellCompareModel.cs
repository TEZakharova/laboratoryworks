﻿namespace Laba2_0.Models
{
    public class CellCompareModel
    {
        public Cell Before { get; set; }

        public Cell After { get; set; }
    }
}
