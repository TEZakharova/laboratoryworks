﻿using System;

namespace Laba2_0.Models
{
    public class Cell : IEquatable<Cell>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SourceThreat { get; set; }
        public string ObjectImpact { get; set; }
        public string PrivacyViolation { get; set; }
        public string IntegrityViolation { get; set; }
        public string AccessViolation { get; set; }
        public string InclusionDate { get; set; }
        public string DateChange { get; set; }

        public bool Equals(Cell other)
        {
            return other != null 
                && this.Name == other.Name 
                //&& this.Description == other.Description
                && this.SourceThreat == other.SourceThreat
                && this.ObjectImpact == other.ObjectImpact
                && this.PrivacyViolation == other.PrivacyViolation
                && this.IntegrityViolation == other.IntegrityViolation
                && this.AccessViolation == other.AccessViolation
                && this.InclusionDate == other.InclusionDate
                && this.DateChange == other.DateChange;
        }

        public override string ToString()
        {
            return $"{Id} {Name}"; //{Description} {SourceThreat} {ObjectImpact} {PrivacyViolation} {IntegrityViolation} {AccessViolation} {InclusionDate} {DateChange}";
        }
    }

}
